package backend.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackendService {
    private final static Logger logger = LoggerFactory.getLogger(BackendService.class.getName());
    public static void main(String[] args) {
        logger.info("Nice, backend service is running");
    }
}
